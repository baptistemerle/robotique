%pour tracer

clear
clc
clf

load data.dat
x=data(:,1);
xt=data(:,4);
t=data(:,7);

figure(1)
plot(t,x,t,xt)
title('poursuite x')

y=data(:,2);
yt=data(:,5);


figure(2)
plot(t,y,t,yt)
title('poursuite y')

th=data(:,3);
tht=data(:,6);


figure(3)
plot(t,th,t,tht)
title('poursuite theta')

-- 
Vous recevez ce message, car vous êtes abonné au groupe Google Groupes GISTRE 2015.
Pour vous désabonner de ce groupe et ne plus recevoir d'e-mails le concernant, envoyez un e-mail à l'adresse gistre-2015+unsubscribe@googlegroups.com.
Pour plus d'options, visitez le site https://groups.google.com/d/optout .
