
all: robot

%.exe: %.cpp
	g++ -I'/usr/include/Aria/' $*.cpp -lAria -lpthread -ldl -lrt -o $@

%.run: %.exe
	./$< -remoteHost 127.0.0.1
