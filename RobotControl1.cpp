/*
18 aout 2012
droite avec un calcul de tht avec atan2 donne un angle -pi, +pi

*/
#include "Aria.h"

#define pi 3.141592654
#define kx 5
#define ky .50
#define kth .10
#define Cycle 20
#define Te Cycle*1e-3
#define a 0.1                      //vitesse de trj d�sir�e

/** @example robotSyncTaskExample.cpp  Shows how to add a task callback to ArRobot's synchronization/processing cycle

  This program will just have the robot wander around, it uses some avoidance 
  routines, then just has a constant velocity.  A sensor interpretation task callback is invoked
  by the ArRobot object every cycle as it runs, which records the robot's current 
  pose and velocity.

  Note that tasks must take a small amount of time to execute, to avoid delaying the
  robot cycle.
  Travail refait le 17 Aout 2012
  trajectoire 1 = droite
*/

class PrintingTask
{
public:
  // Constructor. Adds our 'user task' to the given robot object.
  PrintingTask(ArRobot *robot);

  // Destructor. Does nothing.
  ~PrintingTask(void) {}
  
  // This method will be called by the callback functor
  void doTask(void);
protected:
  ArRobot *myRobot;
  FILE *file;
  int i,j;         // indices utiles
  double xt_1,yt_1;

  // The functor to add to the robot for our 'user task'.
  ArFunctorC<PrintingTask> myTaskCB;
};


// the constructor (note how it uses chaining to initialize myTaskCB)
PrintingTask::PrintingTask(ArRobot *robot) :
  myTaskCB(this, &PrintingTask::doTask)
{
  myRobot = robot;
  // just add it to the robot
  myRobot->addSensorInterpTask("PrintingTask", 50, &myTaskCB);
  file = ArUtil::fopen("data.dat", "w+");
  i=0;
  j=0;xt_1=0;yt_1=0;
}

void PrintingTask::doTask(void)
{
double x,y,th,t;                      //variables de sortie
double ex,ey,eth;                   //les erreurs
double v,w;                         //variables de commande
double vr,wr;                       //consignes de vitesse
double l,xt,yt,tht,tht1;                 //variables dues aux consignes

   wr=0;
   vr=0.05;
   //calcul des trajectoires d�sir�es
   tht=(pi/2);
   t=i*Te;
   l=a*t;
   xt=l*cos(tht1)+0.2;
   yt=l*sin(tht1)+0.1;
   tht1=atan2(yt-yt_1,xt-xt_1);
   xt_1=xt;yt_1=yt;
   i++;
  // r�cup�rer some info about the robot
   x=myRobot->getX()*1e-3;
   y=myRobot->getY()*1e-3; 
   
   th=ArMath::degToRad(myRobot->getTh());

   // calcul des erreurs
   ex= (xt-x)*cos(th)+(yt-y)*sin(th);
   ey= -(xt-x)*sin(th)+(yt-y)*cos(th);
   eth=tht-th;
      
   //calcul des consignes
   v=vr*cos(eth)+kx*ex;
   w=wr+vr*(ky*ey+kth*sin(eth));

   // envoi des consignes
  myRobot->lock();
  myRobot->setRotVel(ArMath::radToDeg(w));
  myRobot->setVel(v*1000);
  myRobot->unlock();
   
  //fprintf(file, "%f, %f, %f, %f, %f, %f, %f\n", x,y,th,xt,yt,tht,t);
  // Need sensor readings? Try myRobot->getRangeDevices() to get all 
  // range devices, then for each device in the list, call lockDevice(), 
  // getCurrentBuffer() to get a list of recent sensor reading positions, then
  // unlockDevice().
}

int main(int argc, char** argv)
{
  // the connection
  ArSimpleConnector con(&argc, argv);
  if(!con.parseArgs())
  {
    con.logOptions();
    return 1;
  }

  // robot
  ArRobot robot;

  // sonar array range device
  ArSonarDevice sonar;

  // This object encapsulates the task we want to do every cycle. 
  // Upon creation, it puts a callback functor in the ArRobot object
  // as a 'user task'.
  PrintingTask pt(&robot);

  // the actions we will use to wander
  ArActionStallRecover recover;
  ArActionAvoidFront avoidFront;
  ArActionConstantVelocity constantVelocity("Constant Velocity", 400);

  // initialize aria
  Aria::init();

  // add the sonar object to the robot
  robot.addRangeDevice(&sonar);

  // open the connection to the robot; if this fails exit
  if(!con.connectRobot(&robot))
  {
    printf("Could not connect to the robot.\n");
    return 2;
  }
  printf("Connected to the robot. (Press Ctrl-C to exit)\n");
  
  
  // turn on the motors, turn off amigobot sounds
  robot.comInt(ArCommands::ENABLE, 1);
  robot.comInt(ArCommands::SOUNDTOG, 0);

  // add the wander actions
  /*robot.addAction(&recover, 100);
  robot.addAction(&avoidFront, 50);
  robot.addAction(&constantVelocity, 25);
  */
  robot.setCycleTime(Cycle);
  // Start the robot process cycle running. Each cycle, it calls the robot's
  // tasks. When the PrintingTask was created above, it added a new
  // task to the robot. 'true' means that if the robot connection
  // is lost, then ArRobot's processing cycle ends and this call returns.
  robot.run(true);

  printf("Disconnected. Goodbye.\n");
  
  return 0;
}
